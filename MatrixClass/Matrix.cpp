#include "Matrix.h"
#include <math.h>

using namespace std;

void gauss();

void jacobi();
Matrix symMatrix(int n);
int* IndexOfMax(Matrix matrix, int n);
float mod(float n);
Matrix rotMatrix(Matrix matrix, int n, int i_max, int j_max);
int sign(float n);

void lin_reg();
float sum_x(Matrix matrix, int n);
float sum_x2(Matrix matrix, int n);
float sum_y(Matrix matrix, int n);
float sum_xy(Matrix matrix, int n);
int randval(int r, int num_iter);

int main() {
	int num;
	cout << "Choose variant:" << endl;
	cout << "Gauss - 1" << endl;
	cout << "Jacobi - 2" << endl;
	cout << "Linear regression - 3" << endl;
	cout << "Input the number of your choice: ";
	cin >> num;
	switch (num) {
	case 1:
		gauss();
		break;
	case 2:
		jacobi();
		break;
	case 3:
		lin_reg();
		break;
	default:
		cout << "Incorrect number!" << endl;
		break;
	}
	system("pause");
	return 0;
}

void gauss() {
	int n;
	cout << "Input a number of equations: ";
	cin >> n;
	Matrix result(n, n+1);
	cout << "Input matrix" << endl;
	cin >> result;
	float  tmp;
	float* xx = new float[n];
	int k, i, j;
	for (i = 0; i < n; i++)
	{
		tmp = result(i, i);
		if (tmp == 0) {
			cout << "The system has no solution!" << endl;
			exit(1);
		}
		for (j = n; j >= i; j--)
			result(i, j) /= tmp;
		for (j = i + 1; j < n; j++)
		{
			tmp = result(j, i);
			for (k = n; k >= i; k--)
				result(j, k) -= tmp * result(i, k);
		}
	}
	xx[n - 1] = result(n - 1, n);
	for (i = n - 2; i >= 0; i--)
	{
		xx[i] = result(i, n);
		for (j = i + 1; j < n; j++) xx[i] -= result(i, j) * xx[j];
	}
	for (i = 0; i < n; i++) {
		
		cout << "x" << i + 1 << " " << "=" << " " << xx[i] << endl;
	}

}

void jacobi() {
	int i_max;
	int j_max;
	int n;
	float eps = 0.1;
	cout << "Input dimension of matrix: ";
	cin >> n;
	Matrix matrix = symMatrix(n);
	cout << "Symmetric matrix:" << endl;
	cout << matrix << endl;
	i_max =  IndexOfMax(matrix, n)[0];
	j_max = IndexOfMax(matrix, n)[1];
	Matrix H(n);
	int counter = 0;
	while (eps < mod(matrix(i_max, j_max))) {
		H = rotMatrix(matrix, n, i_max, j_max);
		matrix = ~H * matrix * H;
		i_max = IndexOfMax(matrix, n)[0];
		j_max = IndexOfMax(matrix, n)[1];
		counter += 1;
		if (counter == 100) {
			break;
		}
	}
	cout << "Eigenvalues of matrix:" << endl;
	for (int i = 0; i < n; i++) {
		cout <<"value #"<<i+1<<" "<<"="<<" "<< matrix(i, i) << endl;
	}

}

int randval(int r, int num_iter) {
	r = mod(r);
	int a = 2;
	int b = 5;
	int m = 10;
	r = (a * r + b) % m;
	if (num_iter % 2) {
		return r;
	}
	else {
		return -r;
	}
}

Matrix symMatrix(int n) {
	Matrix A(n);
	float r = 2;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++) {
			r = randval(r, i * n + j);
			A(i, j) = mod(r);
		}		
	}
	return ~A*A;
}

int* IndexOfMax(Matrix matrix, int n) {
	int index[2];
	float maxVal = 0;
	for (int j = 0; j < n; j++) {
		for (int i = 0; i < j; i++) {
			if (maxVal < mod(matrix(i, j))) {
				maxVal = mod(matrix(i, j));
			}
		}
	}
	for (int j = 0; j < n; j++) {
		for (int i = 0; i < j; i++) {
			if (maxVal == mod(matrix(i, j))) {
				index[0] = i;
				index[1] = j;
				break;
			}
		}
	}
	return index;
}

float mod(float n) {
	if (n < 0) {
		n *= -1;
	}
	return n;
}

int sign(float n) {
	if (n < 0) {
		return -1;
	}
	else {
		return 1;
	}
}

Matrix rotMatrix(Matrix matrix, int n, int i_max, int j_max) {
	Matrix H(n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i == j) {
				H(i, j) = 1;
			}
			else {
				H(i, j) = 0;
			}
		} }
	float p = (2 * matrix(i_max, j_max)) / (matrix(i_max, i_max) - matrix(j_max, j_max));
	float sin = sign(p) * sqrt((1 - 1 / sqrt((1 + p * p)))/2);
	float cos = sqrt((1 + 1 / sqrt((1 + p * p))) / 2);
	H(i_max, i_max) = cos;
	H(j_max, j_max) = cos;
	H(i_max, j_max) = -sin;
	H(j_max, i_max) = sin;
	return H;
}


void lin_reg() {
	float a, b;
	float y;
	int n;
	cout << "Linear function: y = a * x + b" << endl;
	cout << "Input a value of a: ";
	cin >> a;
	cout << "Input a value of b: ";
	cin >> b;
	cout << "Input a number of test points: ";
	cin >> n;
	Matrix matrix(n, 2);
	float r = 2;
	for (int i = 0; i < n; i++) {
		y = a * i + b;
		r = randval(r, i);
		matrix(i, 0) = i;
		matrix(i, 1) = y + r/100;
	}
	cout << "Test points:" << endl;
	cout << matrix << endl;
	float a_res, b_res;
	float s_x, s_y, s_xy, s_x2;
	s_x = sum_x(matrix, n);
	s_y = sum_y(matrix, n);
	s_x2 = sum_x2(matrix, n);
	s_xy = sum_xy(matrix, n);
	a_res = (n * s_xy - s_x * s_y) / (n * s_x2 - s_x * s_x);
	b_res = s_y / n - a_res * s_x / n;
	cout << "Function: " << endl;
	cout << "y = " << a_res << " * x + " << b_res << endl;
}


float sum_x(Matrix matrix, int n) {
	float res = 0;
	for (int i = 0; i < n; i++) {
		res += matrix(i, 0);
	}
	return res;
}

float sum_x2(Matrix matrix, int n) {
	float res = 0;
	for (int i = 0; i < n; i++) {
		res += matrix(i, 0) * matrix(i, 0);
	}
	return res;
}

float sum_y(Matrix matrix, int n) {
	float res = 0;
	for (int i = 0; i < n; i++) {
		res += matrix(i, 1);
	}
	return res;
}

float sum_xy(Matrix matrix, int n) {
	float res = 0;
	for (int i = 0; i < n; i++) {
		res += matrix(i, 0) * matrix(i, 1);
	}
	return res;
}